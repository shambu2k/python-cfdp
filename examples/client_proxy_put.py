import logging

import cfdp
from cfdp.transport import UdpTransport
from cfdp.filestore import NativeFileStore


logging.basicConfig(level=logging.INFO)

config = cfdp.Config(
    local_entity=cfdp.LocalEntity(
        2, "127.0.0.1:5552"),
    remote_entities=[cfdp.RemoteEntity(
        1, "127.0.0.1:5551")],
    filestore=NativeFileStore("./files/client"),
    transport=UdpTransport())

cfdp_entity = cfdp.CfdpEntity(config)

# client needs to bind to a port for server replies
cfdp_entity.transport.bind()

cfdp_entity.put(
    destination_id=1,
    transmission_mode=cfdp.TransmissionMode.ACKNOWLEDGED,
    messages_to_user=[
        cfdp.ProxyPutRequest(
            destination_entity_id=2,
            source_filename="/medium.txt",
            destination_filename="/medium_proxy_put.txt")
            ])

input("Press <Enter> to finish.\n")
cfdp_entity.transport.unbind()
cfdp_entity.shutdown()

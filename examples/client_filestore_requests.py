import logging

import cfdp
from cfdp.transport import UdpTransport
from cfdp.filestore import NativeFileStore


logging.basicConfig(level=logging.DEBUG)

config = cfdp.Config(
    local_entity=cfdp.LocalEntity(
        2, "127.0.0.1:5552"),
    remote_entities=[cfdp.RemoteEntity(
        1, "127.0.0.1:5551")],
    filestore=NativeFileStore("./files/client"),
    transport=UdpTransport())

cfdp_entity = cfdp.CfdpEntity(config)

transaction_id = cfdp_entity.put(
    destination_id=1,
    transmission_mode=cfdp.TransmissionMode.UNACKNOWLEDGED,
    filestore_requests=[
        # directory actions
        cfdp.FilestoreRequest(cfdp.ActionCode.CREATE_DIRECTORY, "/A"),
        cfdp.FilestoreRequest(cfdp.ActionCode.REMOVE_DIRECTORY, "/A"),
        cfdp.FilestoreRequest(cfdp.ActionCode.CREATE_DIRECTORY, "/B"),
        cfdp.FilestoreRequest(cfdp.ActionCode.DENY_DIRECTORY, "/B"),
        # file actions
        cfdp.FilestoreRequest(cfdp.ActionCode.CREATE_FILE, "a.txt"),
        cfdp.FilestoreRequest(cfdp.ActionCode.CREATE_FILE, "b.txt"),
        cfdp.FilestoreRequest(cfdp.ActionCode.APPEND_FILE, "a.txt", "b.txt"),
        cfdp.FilestoreRequest(cfdp.ActionCode.REPLACE_FILE, "a.txt", "b.txt"),
        cfdp.FilestoreRequest(cfdp.ActionCode.DELETE_FILE, "a.txt"),
        cfdp.FilestoreRequest(cfdp.ActionCode.DENY_FILE, "b.txt"),
        ]
    )

input("Press <Enter> to finish.\n")
cfdp_entity.shutdown()


import os
import types
import time

import cfdp
from cfdp.transport import UdpTransport
from cfdp.filestore import NativeFileStore
from cfdp.event import EventType

"""
===============================================================================
                        Test Series F1
===============================================================================
"""


class Server:
    """
    Server used for test clients

    """
    def up(self):
        self.config = cfdp.Config(
            local_entity=cfdp.LocalEntity(
                1, "127.0.0.1:5551"),
            remote_entities=[cfdp.RemoteEntity(
                2, "127.0.0.1:5552",
                nak_timer_interval=1)],
            filestore=NativeFileStore("./files/server"),
            transport=UdpTransport())
        self.cfdp_entity = cfdp.CfdpEntity(self.config)
        self.config.transport.bind()

    def down(self):
        self.config.transport.unbind()
        self.cfdp_entity.shutdown()


"""
===============================================================================
Test Seq 1 - Establish one-way connectivity
===============================================================================
Purpose: Check the one-way connection of the UNACKNOWLEDGED mode

- Establish a one-way connection from client to server
- Send the small.txt file to server

Check: The test is passed, when the file is in the server files.

"""


def test_seq_1():
    server = Server()
    server.up()

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(
            2, "127.0.0.1:5552"),
        remote_entities=[cfdp.RemoteEntity(
            1, "127.0.0.1:5551")],
        filestore=NativeFileStore("./files/client"),
        transport=UdpTransport())
    client = cfdp.CfdpEntity(config)
    transaction_id = client.put(
        destination_id=1,
        source_filename="/small.txt",
        destination_filename="/small.txt",
        transmission_mode=cfdp.TransmissionMode.UNACKNOWLEDGED)

    while not client.is_complete(transaction_id)\
            or not server.cfdp_entity.is_complete(transaction_id):
        time.sleep(0.1)
    client.shutdown()
    server.down()

    assert os.path.isfile('./files/server/small.txt')
    time.sleep(0.1)
    os.remove('./files/server/small.txt')


"""
===============================================================================
Test Seq 2 - Exercise multiple File Data PDUs
===============================================================================
Purpose: Check the possibility to send multiple File Data PDUs
         in UNACKNOWLEDGED mode

- Establish a one-way connection from client to server
- Send the medium.txt file to server

Check: The test is passed, when the medium file is in the server files.

"""


def test_seq_2():
    server = Server()
    server.up()

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(
            2, "127.0.0.1:5552"),
        remote_entities=[cfdp.RemoteEntity(
            1, "127.0.0.1:5551")],
        filestore=NativeFileStore("./files/client"),
        transport=UdpTransport())
    client = cfdp.CfdpEntity(config)
    transaction_id = client.put(
        destination_id=1,
        source_filename="/medium.txt",
        destination_filename="/medium.txt",
        transmission_mode=cfdp.TransmissionMode.UNACKNOWLEDGED)

    while not client.is_complete(transaction_id)\
            or not server.cfdp_entity.is_complete(transaction_id):
        time.sleep(0.1)
    client.shutdown()
    server.down()

    assert os.path.isfile('./files/server/medium.txt')
    time.sleep(0.1)
    os.remove('./files/server/medium.txt')


"""
===============================================================================
Test Seq 3 - Establish two-way connectivity and establish performance baseline
===============================================================================
Purpose: Check the two-way connection of the ACKNOWLEDGED mode and proofe
         the basic functionality

- Establish a two-way connection between client and server
- Send the medium.txt file to server

Check: The test is passed, when the medium file is in the server files.

"""


def test_seq_3():
    server = Server()
    server.up()

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(
            2, "127.0.0.1:5552"),
        remote_entities=[cfdp.RemoteEntity(
            1, "127.0.0.1:5551")],
        filestore=NativeFileStore("./files/client"),
        transport=UdpTransport())
    client = cfdp.CfdpEntity(config)
    client.transport.bind()
    transaction_id = client.put(
        destination_id=1,
        source_filename="/medium.txt",
        destination_filename="/medium.txt",
        transmission_mode=cfdp.TransmissionMode.ACKNOWLEDGED)

    while not client.is_complete(transaction_id)\
            or not server.cfdp_entity.is_complete(transaction_id):
        time.sleep(0.1)
    client.transport.unbind()
    client.shutdown()
    server.down()

    assert os.path.isfile('./files/server/medium.txt')
    time.sleep(0.1)
    os.remove('./files/server/medium.txt')


"""
===============================================================================
Test Seq 4 - Check recovery of dropped data
===============================================================================
Purpose: Check the Recovery of dropped data in the ACKNOWLEDGED mode

- Establish a two-way connection between client and server
- Send the medium.txt file to server
- dropping of ~1% of the File Data PDUs in the sending process

Check: The test is passed, when the medium file in the server files
       is complete.

"""


def test_seq_4():
    server = Server()
    server.up()

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(
            2, "127.0.0.1:5552"),
        remote_entities=[cfdp.RemoteEntity(
            1, "127.0.0.1:5551")],
        filestore=NativeFileStore("./files/client"),
        transport=UdpTransport())
    client = cfdp.CfdpEntity(config)
    client.transport.bind()

    # modify transport request to drop PDUs
    outgoing_pdu_count = 0

    def modified_request(self, data, address):
        nonlocal outgoing_pdu_count
        outgoing_pdu_count += 1

        if outgoing_pdu_count == 5:  # drop this PDU
            return
        if outgoing_pdu_count == 10:  # drop this PDU
            return

        url = address.split(":")
        url[1] = int(url[1])
        self.socket.sendto(data, tuple(url))

    client.transport.request = types.MethodType(
        modified_request, client.transport)

    transaction_id = client.put(
        destination_id=1,
        source_filename="/medium.txt",
        destination_filename="/medium.txt",
        transmission_mode=cfdp.TransmissionMode.ACKNOWLEDGED)

    while not client.is_complete(transaction_id)\
            or not server.cfdp_entity.is_complete(transaction_id):
        time.sleep(0.1)
    client.transport.unbind()
    client.shutdown()
    server.down()

    assert os.path.isfile('./files/server/medium.txt')
    time.sleep(0.1)
    os.remove('./files/server/medium.txt')


"""
===============================================================================
Test Seq 5 - Check deletion of duplicate data
===============================================================================
Purpose: Check deletion of duplicated data.

- Establish a two-way connection between client and server
- Send the medium.txt file to server
- Delete ~1% of the File Data PDUs in the sending process

Check: The test is passed, when the medium file in the server files
       is complete.
"""


def test_seq_5():
    server = Server()
    server.up()

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(
            2, "127.0.0.1:5552"),
        remote_entities=[cfdp.RemoteEntity(
            1, "127.0.0.1:5551")],
        filestore=NativeFileStore("./files/client"),
        transport=UdpTransport())
    client = cfdp.CfdpEntity(config)
    client.transport.bind()

    # modify transport request to duplicate PDUs
    outgoing_pdu_count = 0

    def modified_request(self, data, address):
        nonlocal outgoing_pdu_count
        outgoing_pdu_count += 1

        url = address.split(":")
        url[1] = int(url[1])

        if outgoing_pdu_count == 5:  # duplicate this PDU
            self.socket.sendto(data, tuple(url))
        if outgoing_pdu_count == 10:  # duplicate this PDU
            self.socket.sendto(data, tuple(url))
        self.socket.sendto(data, tuple(url))

    client.transport.request = types.MethodType(
        modified_request, client.transport)

    transaction_id = client.put(
        destination_id=1,
        source_filename="/medium.txt",
        destination_filename="/medium.txt",
        transmission_mode=cfdp.TransmissionMode.ACKNOWLEDGED)

    while not client.is_complete(transaction_id)\
            or not server.cfdp_entity.is_complete(transaction_id):
        time.sleep(0.1)
    client.transport.unbind()
    client.shutdown()
    server.down()

    assert os.path.isfile('./files/server/medium.txt')
    time.sleep(0.1)
    os.remove('./files/server/medium.txt')


"""
===============================================================================
Test Seq 6 - Check reordering of data
===============================================================================
Purpose: Check reording of data sent in the wrong order

- Establish a two-way connection between client and server
- Send the medium.txt file to server
- Sending ~1% of the File Data PDUs out of order

Check: The test is passed, when the medium file in the server files
       is complete and in the right order.
"""


def test_seq_6():
    server = Server()
    server.up()

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(
            2, "127.0.0.1:5552"),
        remote_entities=[cfdp.RemoteEntity(
            1, "127.0.0.1:5551")],
        filestore=NativeFileStore("./files/client"),
        transport=UdpTransport())
    client = cfdp.CfdpEntity(config)
    client.transport.bind()

    # modify transport request to duplicate PDUs
    outgoing_pdu_count = 0
    buffer = []

    def modified_request(self, data, address):
        nonlocal outgoing_pdu_count
        nonlocal buffer
        outgoing_pdu_count += 1

        url = address.split(":")
        url[1] = int(url[1])

        if outgoing_pdu_count == 5:  # pass one PDU and save it for chaos pdu
            buffer.append(data)
            return
        if outgoing_pdu_count == 8:  # pass one PDU and save it for chaos pdu
            time.sleep(1)
            buffer.append(data)
            return
        elif outgoing_pdu_count == 7:  # send the old_data and the current
            self.socket.sendto(buffer.pop(), tuple(url))
            self.socket.sendto(data, tuple(url))
        elif outgoing_pdu_count == 12:  # send the old_data and the current
            self.socket.sendto(buffer.pop(), tuple(url))
            self.socket.sendto(data, tuple(url))
        else:
            self.socket.sendto(data, tuple(url))

    client.transport.request = types.MethodType(
        modified_request, client.transport)

    transaction_id = client.put(
        destination_id=1,
        source_filename="/medium.txt",
        destination_filename="/medium.txt",
        transmission_mode=cfdp.TransmissionMode.ACKNOWLEDGED)

    while not client.is_complete(transaction_id)\
            or not server.cfdp_entity.is_complete(transaction_id):
        time.sleep(0.1)
    client.transport.unbind()
    client.shutdown()
    server.down()

    assert os.path.isfile('./files/server/medium.txt')
    time.sleep(0.1)
    os.remove('./files/server/medium.txt')


"""
===============================================================================
Test Seq 7_1 - Check user (application) messages functioning - ProxyPutRequest
===============================================================================
Purpose: Check the functioning of 2 user (application) messages.
         In the first case check of the ProxyPutRequest

- Establish a two-way connection between client and server
- Send the ProxyPutRequest to the server and request the medium.txt
- Sending of the medium.txt from server to client

Check: The test is passed, when the medium file exists in client files.

"""


def test_seq_7_1():
    server = Server()
    server.up()

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(
            2, "127.0.0.1:5552"),
        remote_entities=[cfdp.RemoteEntity(
            1, "127.0.0.1:5551")],
        filestore=NativeFileStore("./files/client"),
        transport=UdpTransport())
    client = cfdp.CfdpEntity(config)
    client.transport.bind()

    transaction_id = client.put(
        destination_id=1,
        transmission_mode=cfdp.TransmissionMode.ACKNOWLEDGED,
        messages_to_user=[
            cfdp.ProxyPutRequest(
                destination_entity_id=2,
                source_filename="/remote_medium.txt",
                destination_filename="/local_medium.txt")])

    time.sleep(1)  # wait for proxy operation to start
    while not client.is_complete(transaction_id)\
            or not server.cfdp_entity.is_complete(transaction_id):
        time.sleep(0.1)
    time.sleep(1)
    client.transport.unbind()
    client.shutdown()
    server.down()

    assert os.path.isfile('./files/client/local_medium.txt')
    time.sleep(0.1)
    os.remove('./files/client/local_medium.txt')


"""
===============================================================================
Test Seq 7_2 - Check user (application) messages functioning - Cancel
===============================================================================
Purpose: Check the functioning of 2 user (application) messages.
         In the second case check the ProxyPutCancel

- Establish a two-way connection between client and server
- Send the ProxyPutRequest to the server and request the medium.txt
- Sending of the medium.txt from server to client
- Send the ProxyPutCancel to the server

Check: The test is passed, when the medium file doesn´t exist in client
       files, because of canceling.

"""


def test_seq_7_2():
    server = Server()
    server.up()

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(
            2, "127.0.0.1:5552"),
        remote_entities=[cfdp.RemoteEntity(
            1, "127.0.0.1:5551")],
        filestore=NativeFileStore("./files/client"),
        transport=UdpTransport())
    client = cfdp.CfdpEntity(config)
    client.transport.bind()

    client.put(
        destination_id=1,
        transmission_mode=cfdp.TransmissionMode.ACKNOWLEDGED,
        messages_to_user=[
            cfdp.ProxyPutRequest(
                destination_entity_id=2,
                source_filename="/remote_medium.txt",
                destination_filename="/local_medium.txt")])

    transaction_id = None
    while transaction_id is None:
        transaction_ids = list(client.machines.keys())
        for n in transaction_ids:
            if n[0] == 1:
                transaction_id = n
                break  # transfer from remote has started

    client.put(
        destination_id=1,
        transmission_mode=cfdp.TransmissionMode.ACKNOWLEDGED,
        messages_to_user=[cfdp.ProxyPutCancel(*transaction_id)])

    print("Transaction cancelled.")
    time.sleep(1)

    while not client.is_complete(transaction_id)\
            or not server.cfdp_entity.is_complete(transaction_id):
        time.sleep(0.1)
    client.transport.unbind()
    client.shutdown()
    server.down()

    time.sleep(0.1)
    assert not os.path.isfile('./files/client/local_medium.txt')


"""
===============================================================================
Test Seq 8 - Check cancel functioning (Sender initiated
approximately mid-file)
===============================================================================
Purpose: Check the sender initiated cancel functioning of ACKNOWLEDGED mode.

- Establish a two-way connection between client and server
- Send the medium.txt file to server
- approximately mid-file canceling the sending process from sender/client side

Check: The test is passed, when the medium file doesn´t exist in Sender
       files, because of canceling.
"""


def test_seq_8():
    server = Server()
    server.up()

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(
            2, "127.0.0.1:5552"),
        remote_entities=[cfdp.RemoteEntity(
            1, "127.0.0.1:5551")],
        filestore=NativeFileStore("./files/client"),
        transport=UdpTransport())
    client = cfdp.CfdpEntity(config)
    client.transport.bind()

    transaction_id = client.put(
        destination_id=1,
        source_filename="/large.txt",
        destination_filename="/local_large.txt",
        transmission_mode=cfdp.TransmissionMode.ACKNOWLEDGED)

    # modify the process of sending data
    # cancel function within the 8 File Data PDU
    file_data_count = 0

    def modified_trigger_event(self, event, pdu=None):
        nonlocal file_data_count
        self._event_queue.put([event, pdu])
        if event.type == EventType.E1_SEND_FILE_DATA:
            file_data_count += 1
            if file_data_count == 8:
                cfdp.logger.info("Transaction cancelled.")
                client.cancel(transaction_id)

    client.trigger_event = types.MethodType(
        modified_trigger_event, client)

    while not client.is_complete(transaction_id)\
            or not server.cfdp_entity.is_complete(transaction_id):
        time.sleep(0.1)
    client.transport.unbind()
    client.shutdown()
    server.down()

    time.sleep(0.1)
    assert not os.path.isfile('./files/server/local_large.txt')


"""
===============================================================================
Test Seq 9 - Check cancel functioning (Receiver initiated
approximately mid-file)
===============================================================================
Purpose: Check the Receiver initiated cancel functioning of ACKNOWLEDGED mode.

- Establish a two-way connection between client and server
- Send the medium.txt file to server
- approximately mid-file canceling the sending process from
  server/Receiver side

Check: The test is passed, when the medium file doesn´t exist in Sender
       files, because of canceling.
"""


def test_seq_9():
    server = Server()
    server.up()

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(
            2, "127.0.0.1:5552"),
        remote_entities=[cfdp.RemoteEntity(
            1, "127.0.0.1:5551")],
        filestore=NativeFileStore("./files/client"),
        transport=UdpTransport())
    client = cfdp.CfdpEntity(config)
    client.transport.bind()

    transaction_id = client.put(
        destination_id=1,
        source_filename="/large.txt",
        destination_filename="/local_large.txt",
        transmission_mode=cfdp.TransmissionMode.ACKNOWLEDGED)

    # modify the process of sending data
    # cancel function within the 8 File Data PDU
    file_data_count = 0

    def modified_trigger_event(self, event, pdu=None):
        nonlocal file_data_count
        self._event_queue.put([event, pdu])
        if event.type == EventType.E11_RECEIVED_FILEDATA:
            file_data_count += 1
            if file_data_count == 8:
                cfdp.logger.info("Transaction cancelled.")
                server.cfdp_entity.cancel(transaction_id)

    server.cfdp_entity.trigger_event = types.MethodType(
        modified_trigger_event, server.cfdp_entity)

    while not client.is_complete(transaction_id)\
            or not server.cfdp_entity.is_complete(transaction_id):
        time.sleep(0.1)
    client.transport.unbind()
    client.shutdown()
    server.down()

    time.sleep(0.1)
    assert not os.path.isfile('./files/server/local_large.txt')


"""
===============================================================================
Test Seq 10 - Check cancel functioning (Sender initiated
approximately mid-file)
===============================================================================
Purpose: Check the sender initiated cancel functioning of UNACKNOWLEDGED mode.

- Establish a one-way connection from client to server
- Send the medium.txt file to server
- approximately mid-file canceling the sending process from sender/client side

Check: The test is passed, when the medium file doesn´t exist in Sender
       files, because of canceling.
"""


def test_seq_10():
    server = Server()
    server.up()

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(
            2, "127.0.0.1:5552"),
        remote_entities=[cfdp.RemoteEntity(
            1, "127.0.0.1:5551")],
        filestore=NativeFileStore("./files/client"),
        transport=UdpTransport())
    client = cfdp.CfdpEntity(config)
    client.transport.bind()

    transaction_id = client.put(
        destination_id=1,
        source_filename="/medium.txt",
        destination_filename="/local_medium.txt",
        transmission_mode=cfdp.TransmissionMode.UNACKNOWLEDGED)

    # modify the process of sending data
    # cancel function within the 8 File Data PDU
    file_data_count = 0

    def modified_trigger_event(self, event, pdu=None):
        nonlocal file_data_count
        self._event_queue.put([event, pdu])
        if event.type == EventType.E1_SEND_FILE_DATA:
            file_data_count += 1
            if file_data_count == 8:
                cfdp.logger.info("Transaction cancelled.")
                client.cancel(transaction_id)

    client.trigger_event = types.MethodType(
        modified_trigger_event, client)

    while not client.is_complete(transaction_id)\
            or not server.cfdp_entity.is_complete(transaction_id):
        time.sleep(0.1)
    client.transport.unbind()
    client.shutdown()
    server.down()

    time.sleep(0.1)
    assert not os.path.isfile('./files/server/local_medium.txt')


if __name__ == "__main__":
    import logging
    logging.basicConfig(level=logging.DEBUG)

    print("Test Seq 1 " + 50 * "=")
    test_seq_1()

    print("Test Seq 2 " + 50 * "=")
    test_seq_2()

    print("Test Seq 3 " + 50 * "=")
    test_seq_3()

    print("Test Seq 4 " + 50 * "=")
    test_seq_4()

    print("Test Seq 5 " + 50 * "=")
    test_seq_5()

    print("Test Seq 6 " + 50 * "=")
    test_seq_6()

    print("Test Seq 7_1 " + 50 * "=")
    test_seq_7_1()

    print("Test Seq 7_2 " + 50 * "=")
    test_seq_7_2()

    print("Test Seq 8 " + 50 * "=")
    test_seq_8()

    print("Test Seq 9 " + 50 * "=")
    test_seq_9()

    print("Test Seq 10 " + 50 * "=")
    test_seq_10()
